﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IraleeMsg
{
    class Program
    {
        static void Main(string[] args)
        {
            String pathArquivo = args[0];
            StreamReader leitor = File.OpenText(pathArquivo);
            List<int> linhafinal = new List<int>();
            List<string> Linhas = new List<string>();
            var EncontrouFrom = false;
            while (leitor.Peek() > -1)
            {
                String LinhaFrom = "";
                String Segundalinha = "";
                string linha = leitor.ReadLine();
                if (linha.StartsWith("From "))
                {
                        linhafinal.Add(Linhas.Count);
                        Linhas.Add(linha);
                }
                else
                    Linhas.Add(linha);
                
            }
            leitor.Close();

            var tamanho = 0;
           
            List<string> emails = new List<string>();
            while (tamanho < (linhafinal.Count-1))
            {
                StringBuilder email = new StringBuilder();
                var indice = linhafinal[tamanho];
                var count = (linhafinal[tamanho + 1] - linhafinal[tamanho]);
                List<string> linhasdoEmail = Linhas.GetRange(indice,count   );
                foreach (var linha in linhasdoEmail)
                {
                    email.AppendLine(linha);
                }
                emails.Add(email.ToString());
                tamanho += 1;
            }
            var qtde = 1;
            foreach (var item in emails)
            {

                qtde++;
                StreamWriter arquivo = new StreamWriter(
                string.Format(@"{0}\{1}_{2}.txt",    args[1], item.Split(' ')[1], qtde) );
                arquivo.Write(item);
                arquivo.Close();
                arquivo.Close();
            }
            
        }
    }
}
